import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { addToCart,removeItem,addQuantity,subtractQuantity } from '../actions/cartActions'
 class Home extends Component{
    constructor(props) {
        super(props);
        this.state = {
          isAdded: false
        };
      }
      //to remove the item completely
    handleRemove = (id)=>{
        this.props.removeItem(id);
    }
    //to add the quantity
    handleAddQuantity = (id)=>{
        this.props.addQuantity(id);
    }
    //to substruct from the quantity
    handleSubtractQuantity = (id)=>{
        this.props.subtractQuantity(id);
    }
    handleClick(id){
        let usertoken='satish'
        console.log("id",id);
        localStorage.setItem("token",usertoken)
        this.props.addToCart(id); 
            this.setState(
                {
                  isAdded: true
                },
                function() {
                  setTimeout(() => {
                    this.setState({
                        isAdded: false,
                    });
                  }, 1500);
                }
          );     
    }
    render(){   
        var token=localStorage.getItem("token")
        let itemList = this.props.items.map(item=>{
            return(
                <div className="card" key={item.id}>
                        <div className="card-image">
                            <img src={item.img} alt={item.title}/>
                            <span className="card-title">{item.title}</span>
                            <button key={item.id} className={!(this.state.isAdded && item.id )? "btn-floating halfway-fab waves-effect waves-light red" : "btn-floating halfway-fab waves-effect waves-light green"} onClick={()=>{this.handleClick(item.id)}}><i className="material-icons">   {!(this.state.isAdded &&item.id )? "add": "remove"}</i></button>                 
                        </div>
                        <div className="card-content">
                            <p>{item.desc}</p>
                            <p><b>Price: {item.price}$</b></p>
                            <div className="item-desc">
                                        <span className="title">{item.title}</span>

                                        <p>
                                            <b>Quantity: {item.quantity}</b> 
                                        </p>
                                        <div className="add-remove">
                                         
                                        </div>
                                  </div>
                        </div>
                 </div>
            )
        })
        return(
            <div className="container">
                <h3 className="center">Our items</h3>
                <div className="box">
                    {itemList}
                </div>
            </div>
        )
    }
}
const mapStateToProps = (state)=>{
    return {
      items: state.items,
    }
  }
const mapDispatchToProps= (dispatch)=>{
    return{
        addToCart: (id)=>{dispatch(addToCart(id))},
        removeItem: (id)=>{dispatch(removeItem(id))},
        addQuantity: (id)=>{dispatch(addQuantity(id))},
        subtractQuantity: (id)=>{dispatch(subtractQuantity(id))}
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Home)


