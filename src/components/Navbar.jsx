import React, { Component } from 'react';
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
class Navbar extends Component {
    constructor(props){
        super(props)
        this.state={
        }
    }
    render() {
        return (
                   <nav className="nav-wrapper">
                        <div className="container">
                             <Link to="/" className="brand-logo">shopNICcsfafaeffasffs</Link>
                             <ul className="right">
                                <li><Link to="/">Shop</Link></li>
                                <li><Link to="/cart">My cart</Link></li>
                                <li><Link to="/cart"><i className="material-icons">shopping_cart</i></Link></li>
                             </ul>
                               <div className="right">
                               <p>total price  {this.props.total}</p>
                               </div>
                        </div>
                   </nav>
              )
         }
  }
  const mapStateToProps = (state)=>{
    return {
      total: state.total,
      addedItems:state.addedItems
    }
  }
  export default connect(mapStateToProps,null)(Navbar);