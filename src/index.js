import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './index.css'
import cartReducer from './reducers/cartReducer';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import 'bootstrap/dist/css/bootstrap.css' 
const store = createStore(cartReducer);
console.log("DXD",cartReducer)
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'));
