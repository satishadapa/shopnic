
import { ADD_TO_CART,REMOVE_ITEM,SUB_QUANTITY,ADD_QUANTITY,ADD_SHIPPING } from '../actions/actiontypes.js'
const initState = {
    items: [
        {id:1,title:'Winter body', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:110,img:"https://static.toiimg.com/thumb/msid-61654288,width-640,resizemode-4/61654288.jpg"},
        {id:2,title:'Adidas', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:80,img: "https://cdn.vox-cdn.com/thumbor/9bUALftOAzKL_DiY4L-HffLwUCs=/0x0:2640x1748/1200x800/filters:focal(1109x663:1531x1085)/cdn.vox-cdn.com/uploads/chorus_image/image/60675421/twarren_iphonesim_1.1533038365.jpg"},
        {id:3,title:'Vans', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",price:120,img: "https://ksassets.timeincuk.net/wp/uploads/sites/54/2017/11/Galaxy-S8-1024x576.png"},
        {id:4,title:'White', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:260,img:"https://img.purch.com/vivo-nex-s/w/755/aHR0cDovL21lZGlhLmJlc3RvZm1pY3JvLmNvbS9PL0cvNzg4ODQ4L29yaWdpbmFsL1Zpdm8tTmV4LVMtMDA2LmpwZw=="},
        {id:5,title:'Cropped-sho', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.", price:160,img:"https://cdn.cnn.com/cnnnext/dam/assets/190220131706-samsung-galaxy-s10-front-exlarge-169.jpg"},
        {id:6,title:'Blues', desc: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Minima, ex.",price:90,img: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR4YV6rTv9HvBgBduEyWfQF4EAOLSmpvuj83VZvOa6FIUFBvuQa"}
    ],
    addedItems:[],
    total: 0,
    name:"satish",
    isAdded: false
}
const cartReducer= (state = initState,action)=>{
    //INSIDE HOME COMPONENT
    if(action.type === ADD_TO_CART){
          let addedItem = state.items.find(item=> item.id === action.id)
         let existed_item= state.addedItems.find(item=> action.id === item.id)
         if(existed_item)
         {
            addedItem.quantity += 1 
             return{
                ...state,
                 total: state.total + addedItem.price ,
                  isAdded: false
              
                  }
        }
         else{
            addedItem.quantity = 1;
            //calculating the total
            let newTotal = state.total + addedItem.price 
            return{ 
                ...state,
                addedItems: [...state.addedItems, addedItem],
                total : newTotal,
            }
        }
    }
    if(action.type === REMOVE_ITEM){
        let itemToRemove= state.addedItems.find(item=> action.id === item.id)
        let new_items = state.addedItems.filter(item=> action.id !== item.id)
        
        //calculating the total
        let newTotal = state.total - (itemToRemove.price * itemToRemove.quantity )
        console.log(itemToRemove)
        return{
            ...state,
            addedItems: new_items,
            total: newTotal
        }
    }

    //INSIDE CART COMPONENT
    if(action.type=== ADD_QUANTITY){
        let addedItem = state.items.find(item=> item.id === action.id)
          addedItem.quantity += 1 
          let newTotal = state.total + addedItem.price
          return{
              ...state,
              total: newTotal
        }
    }
    if(action.type=== SUB_QUANTITY){     
        let addedItem = state.items.find(item=> item.id === action.id) 
        //if the qt == 0 then it should be removed
        if(addedItem.quantity === 1){
            let new_items = state.addedItems.filter(item=>item.id !== action.id)
            let newTotal = state.total - addedItem.price
            return {
                ...state,
                addedItems: new_items,
                total: newTotal
            }
        }
        else {
            addedItem.quantity -= 1
            let newTotal = state.total - addedItem.price
            return{
                ...state,
                total: newTotal
            }
      }  
    }
    if(action.type=== ADD_SHIPPING){
          return{
              ...state,
              total: state.total + 6
          }
    }
    if(action.type=== 'SUB_SHIPPING'){
        return {
            ...state,
            total: state.total - 6
        }
  }
  if(action.type==='CHECK_OUT'){
       return {
           ...state,
           total:state.total
       }

  }
    return state
}
export default cartReducer